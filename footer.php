<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<?php
			do_action('login_link');
		?>

		<div id="footer">
		<ul id="footer-info">

			<li><a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<?php bloginfo( 'name' ); ?>
			</a></li>  
			<li>
				<a href="tel://<?php echo get_option('phone_number'); ?>"><?php echo get_option('phone_number'); ?></a>
			</li>
			<li><a href="mailto:<?php bloginfo('admin_email'); ?>?subject=Website" target="_blank">
				<?php bloginfo('admin_email'); ?>
			</a></li>  
			<?php 
			$address = get_option('address');
			$google_address = explode(' ', $address);
			$google_address = implode('+', $google_address);
			?>
			<li><a href="https://maps.google.ca/maps?q=<?php echo $google_address; ?>" target="_blank">
				<?php echo $address; ?>
			</a></li>
		</ul>
		</div>

	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>