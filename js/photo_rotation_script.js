(function($) {
    var container = $('#gallery-1'),
        imgs = container.find('img'),
        len = imgs.length,
        i = 0,
        delay = 5000,
        duration = 1000,
        width = container.width(),
        height = container.height(),
        numberOfBlinds = 20,
        margin = 1,
        color = '#000',
        gapHeight = 100,
        blindWidth = 100 / numberOfBlinds;

    $(window).on('resize', function () {
        width = container.width();
        height = container.height();
    });

    function calculateBorders() {
        var random = Math.floor((Math.random() * 9) + 1),
            borderWidthTop = (random / 10) * gapHeight,
            borderWidthBottom = gapHeight - borderWidthTop;

        return {
            borderWidthTop: borderWidthTop,
            borderWidthBottom: borderWidthBottom
        };
    }

    imgs.eq(i).show();
    
    for (var k = 0; k < numberOfBlinds; k++) {
        var tempEl = $('<span>');
        var borders = calculateBorders();

        tempEl.css({
            'left': (k * blindWidth) + '%',
            'borderTopWidth': borders.borderWidthTop,
            'borderBottomWidth': borders.borderWidthBottom,
            'height': height,
            'width': blindWidth + '%'
        });

        container.prepend(tempEl);
    };
    var blinds = $('span', container);

    function animateBorders() {
        blinds.animate({
            borderTopWidth: height / 2,
            borderBottomWidth: height / 2
        }, duration, function() {
            var borders = calculateBorders();
            $(this).animate({
                borderTopWidth: borders.borderWidthTop,
                borderBottomWidth: borders.borderWidthBottom
            }, duration);
            clearTimeout(t);
            t = setTimeout(animateBorders, delay);
        });
        setTimeout(function () {
            var j = (i === (len - 1)) ? 0 : i + 1,
                current = imgs.eq(i),
                nextUp = imgs.eq(j);
            //global
            i = (j) ? i+1 : 0;
            current.hide();
            nextUp.show();
        }, duration);
    }

    var t = setTimeout(animateBorders, delay);

})(jQuery);
